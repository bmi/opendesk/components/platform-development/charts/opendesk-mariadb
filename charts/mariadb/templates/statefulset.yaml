{{/*
SPDX-FileCopyrightText: 2024 Zentrum für Digitale Souveränität der Öffentlichen Verwaltung (ZenDiS) GmbH
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
*/}}
---
apiVersion: {{ include "common.capabilities.statefulset.apiVersion" . }}
kind: "StatefulSet"
metadata:
  name: {{ include "common.names.fullname" . }}
  namespace: {{ include "common.names.namespace" . | quote }}
  labels: {{- include "common.labels.standard" . | nindent 4 }}
    {{- if .Values.commonLabels }}
    {{- include "common.tplvalues.render" ( dict "value" .Values.commonLabels "context" . ) | nindent 4 }}
    {{- end }}
  {{- if .Values.commonAnnotations }}
  annotations: {{- include "common.tplvalues.render" ( dict "value" .Values.commonAnnotations "context" . ) | nindent 4 }}
  {{- end }}
spec:
  replicas: {{ .Values.replicaCount }}
  serviceName: {{ include "common.names.fullname" . }}
  {{- if .Values.updateStrategy }}
  updateStrategy: {{ include "common.tplvalues.render" (dict "value" .Values.updateStrategy "context" . ) | nindent 4 }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "common.labels.matchLabels" . | nindent 6 }}
  template:
    metadata:
      {{- if or .Values.mariadb.configuration .Values.podAnnotations }}
      annotations:
        {{- if .Values.mariadb.configuration }}
        checksum/configuration: {{ include (print .Template.BasePath "/configmap.yaml") . | sha256sum }}
        {{- end }}
        {{- if .Values.podAnnotations }}
        {{- include "common.tplvalues.render" (dict "value" .Values.podAnnotations "context" . ) | nindent 8 }}
        {{- end }}
      {{- end }}
      labels:
        {{- include "common.labels.standard" . | nindent 8 }}
    spec:
      {{- if .Values.extraPodSpec }}
      {{- include "common.tplvalues.render" (dict "value" .Values.extraPodSpec "context" . ) | nindent 6 }}
      {{- end }}
      {{- if .Values.priorityClassName }}
      priorityClassName: {{ .Values.priorityClassName }}
      {{- end }}
      {{- if .Values.schedulerName }}
      schedulerName: {{ .Values.schedulerName | quote }}
      {{- end }}
      {{- if or .Values.imagePullSecrets .Values.global.imagePullSecrets }}
      imagePullSecrets:
        {{- range .Values.global.imagePullSecrets }}
        - name: {{ . | quote }}
        {{- end }}
        {{- range .Values.imagePullSecrets }}
        - name: {{ . | quote }}
        {{- end }}
      {{- end }}
      {{- if .Values.affinity }}
      affinity: {{- include "common.tplvalues.render" (dict "value" .Values.affinity "context" . ) | nindent 8 }}
      {{- end }}
      {{- if .Values.tolerations }}
      tolerations: {{- include "common.tplvalues.render" (dict "value" .Values.tolerations "context" . ) | nindent 8 }}
      {{- end }}
      {{- if .Values.topologySpreadConstraints }}
      topologySpreadConstraints: {{- include "common.tplvalues.render" (dict "value" .Values.topologySpreadConstraints "context" . ) | nindent 8 }}
      {{- end }}
      {{- if .Values.nodeSelector }}
      nodeSelector: {{- include "common.tplvalues.render" (dict "value" .Values.nodeSelector "context" . ) | nindent 8 }}
      {{- end }}
      {{- if .Values.podSecurityContext.enabled }}
      securityContext: {{- omit .Values.podSecurityContext "enabled" | toYaml | nindent 8 }}
      {{- end }}
      {{- if .Values.serviceAccount.create }}
      serviceAccountName: {{ include "common.names.fullname" . }}
      {{- end }}
      {{- if .Values.terminationGracePeriodSeconds }}
      terminationGracePeriodSeconds: {{ .Values.terminationGracePeriodSeconds }}
      {{- end }}
      hostNetwork: {{ .Values.hostNetwork }}
      hostIPC: {{ .Values.hostIPC }}
      containers:
        - name: "mariadb"
          {{- if .Values.containerSecurityContext.enabled }}
          securityContext: {{- omit .Values.containerSecurityContext "enabled" | toYaml | nindent 12 }}
          {{- end }}
          image: {{ printf "%s/%s:%s" (coalesce .Values.image.registry .Values.global.registry) .Values.image.repository .Values.image.tag | quote }}
          imagePullPolicy: {{ .Values.image.imagePullPolicy }}
          env:
            - name: "MARIADB_ROOT_PASSWORD"
              valueFrom:
                secretKeyRef:
                  name: {{ .Values.mariadb.rootPassword.existingSecret.name | default (printf "%s" (include "common.names.fullname" .)) }}
                  key: {{ .Values.mariadb.rootPassword.existingSecret.key | default "rootPassword" }}
            {{- if .Values.extraEnvVars }}
            {{- include "common.tplvalues.render" (dict "value" .Values.extraEnvVars "context" . ) | nindent 12 }}
            {{- end }}
          {{- if or .Values.extraEnvVarsCM .Values.extraEnvVarsSecret }}
          envFrom:
            {{- if .Values.extraEnvVarsCM }}
            - configMapRef:
                name: {{ .Values.extraEnvVarsCM }}
            {{- end }}
            {{- if .Values.extraEnvVarsSecret }}
            - secretRef:
                name: {{ .Values.extraEnvVarsSecret }}
            {{- end }}
          {{- end }}
          ports:
            {{- range $key, $value := .Values.service.ports }}
            - name: {{ $key }}
              containerPort: {{ $value.containerPort }}
              protocol: {{ $value.protocol }}
            {{- end }}
          {{- if .Values.startupProbe.enabled }}
          startupProbe: {{- include "common.tplvalues.render" (dict "value" (omit .Values.startupProbe "enabled") "context" . ) | nindent 12 }}
            exec:
              command:
                - /bin/sh
                - -ec
                - mariadb-admin status -uroot -p"${MARIADB_ROOT_PASSWORD}"
          {{- end }}
          {{- if .Values.livenessProbe.enabled }}
          livenessProbe: {{- include "common.tplvalues.render" (dict "value" (omit .Values.livenessProbe "enabled") "context" . ) | nindent 12 }}
            exec:
              command:
                - /bin/sh
                - -ec
                - mariadb-admin status -uroot -p"${MARIADB_ROOT_PASSWORD}"
          {{- end }}
          {{- if .Values.readinessProbe.enabled }}
          readinessProbe: {{- include "common.tplvalues.render" (dict "value" (omit .Values.readinessProbe "enabled") "context" . ) | nindent 12 }}
            exec:
              command:
                - /bin/sh
                - -ec
                - mariadb-admin status -uroot -p"${MARIADB_ROOT_PASSWORD}"
          {{- end }}
          {{- if .Values.resources }}
          resources: {{- include "common.tplvalues.render" (dict "value" .Values.resources "context" . ) | nindent 12 }}
          {{- end }}
          {{- if .Values.lifecycleHooks }}
          lifecycle: {{- include "common.tplvalues.render" (dict "value" .Values.lifecycleHooks "context" . ) | nindent 12 }}
          {{- end }}
          volumeMounts:
            - name: "tmp"
              mountPath: "/tmp"
            - name: "run-mysqld"
              mountPath: "/run/mysqld"
            {{- if .Values.persistence.enabled }}
            - name: "data"
              mountPath: "/var/lib/mysql"
            {{- end }}
            {{- if .Values.mariadb.configuration }}
            - name: "mariadb-config"
              mountPath: "/etc/mysql/conf.d/custom.cnf"
            {{- end }}
            {{- if .Values.extraVolumeMounts }}
            {{- include "common.tplvalues.render" (dict "value" .Values.extraVolumeMounts "context" . ) | nindent 12 }}
            {{- end }}
      volumes:
        - name: "tmp"
          emptyDir: {}
        - name: "run-mysqld"
          emptyDir: {}
        {{- if .Values.mariadb.configuration }}
        - name: "mariadb-config"
          configMap:
            name: {{ include "common.names.fullname" . }}
        {{- end }}
        {{- if .Values.extraVolumes }}
        {{- include "common.tplvalues.render" ( dict "value" .Values.extraVolumes "context" . ) | nindent 8 }}
        {{- end }}
  {{- if and .Values.persistence.enabled .Values.persistence.existingClaim }}
        - name: "data"
          persistentVolumeClaim:
            claimName: {{ tpl .Values.persistence.existingClaim . }}
  {{- else if not .Values.persistence.enabled }}
        - name: "data"
          emptyDir: {}
  {{- else }}
  volumeClaimTemplates:
    - metadata:
        name: data
        {{- if .Values.persistence.annotations }}
        annotations: {{- include "common.tplvalues.render" (dict "value" .Values.persistence.annotations "context" . ) | nindent 10 }}
        {{- end }}
        {{- if .Values.persistence.labels }}
        labels: {{- include "common.tplvalues.render" (dict "value" .Values.persistence.labels "context" . ) | nindent 10 }}
        {{- end }}
      spec:
        accessModes:
        {{- range .Values.persistence.accessModes }}
          - {{ . | quote }}
        {{- end }}
        {{- if .Values.persistence.dataSource }}
        dataSource: {{- include "common.tplvalues.render" (dict "value" .Values.persistence.dataSource "context" . ) | nindent 10 }}
        {{- end }}
        resources:
          requests:
            storage: {{ .Values.persistence.size | quote }}
        {{- if .Values.persistence.selector }}
        selector: {{- include "common.tplvalues.render" (dict "value" .Values.persistence.selector "context" . ) | nindent 10 }}
        {{- end }}
        {{- include "common.storage.class" (dict "persistence" .Values.persistence "global" .Values.global) | nindent 8 }}
  {{- end }}
...
